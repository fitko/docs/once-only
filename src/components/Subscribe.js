import React, { useState } from 'react'

export default function Subscribe({children, formPostUrl}) {
    const [showChildren, setShowChildren] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);

    const handleSkip = (e) => {
        e.preventDefault();
        setShowChildren(!showChildren);
    }

    const handleSubmit = (e) => {
        setFormSubmitted(!showChildren);
    }

    return (
        <form method="POST" action={formPostUrl} className="subscribe-form" onSubmit={handleSubmit}>
            <label htmlFor="fullname">Name</label>
            <input type="text" name="fullname" placeholder="(optional)" maxLength="64"></input><br />

            <label htmlFor="email">E-Mail</label>
            <input type="text" required name="email" placeholder="beispiel@example.com" maxLength="64"></input><br />

            <input type="submit" disabled={formSubmitted} className="get-subscribe" name="email-button" value="Abonnieren"></input>
            <a href="#" className="no-subscribe" onClick={handleSkip}>
                {showChildren ? "Abbrechen" : "Nicht abonnieren"}
            </a>
            {
                showChildren || formSubmitted ? <>
                    <br/>
                    {children}
                </> : ''
            }
        </form>
    )
}
